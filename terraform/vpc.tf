
module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.1.2"

  name                   = var.vpc_name
  cidr                   = var.vpc_cidr
  azs                    = data.aws_availability_zones.available.names
  public_subnets         = ["10.20.1.0/24", "10.20.2.0/24", "10.20.3.0/24"]
  enable_nat_gateway     = false
  single_nat_gateway     = false
  one_nat_gateway_per_az = false
  enable_dns_hostnames   = true

  tags = {
    "Name" = "${var.vpc_name}"
  }
  public_subnet_tags = {
    "Name" = "public-subnet-${var.vpc_name}"
  }
  public_route_table_tags = {
    "Name" = "public-route-table-${var.vpc_name}"
  }
}
