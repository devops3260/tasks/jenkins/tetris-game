#!/bin/bash

sudo apt update -y
#sudo apt upgrade -y
# Jenkins key
curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key | sudo tee  /usr/share/keyrings/jenkins-keyring.asc > /dev/null
echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc]  https://pkg.jenkins.io/debian-stable binary/ | sudo tee   /etc/apt/sources.list.d/jenkins.list > /dev/null
# trivy key
wget -qO - https://aquasecurity.github.io/trivy-repo/deb/public.key | gpg --dearmor | sudo tee /usr/share/keyrings/trivy.gpg > /dev/null
echo "deb [signed-by=/usr/share/keyrings/trivy.gpg] https://aquasecurity.github.io/trivy-repo/deb $(lsb_release -sc) main" | sudo tee -a /etc/apt/sources.list.d/trivy.list
# Install Java
sudo apt update -y
sudo apt install openjdk-17-jdk  wget apt-transport-https gnupg lsb-release -y
/usr/bin/java --version

# Install Jenkins, Docker and trivy
sudo apt install jenkins docker.io trivy -y
sudo systemctl start jenkins

sudo usermod -aG docker $USER   #my case is ubuntu
sudo chmod 777 /var/run/docker.sock
newgrp docker


# Start sonarqube
docker run -d --name sonar -p 9000:9000 sonarqube:lts-community
