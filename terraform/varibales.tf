variable "aws_region" {
  description = "AWS Region."
  default     = "us-east-1"
}

variable "default_tags" {
  type        = map(string)
  description = "Default tags for AWS that will be attached to each resource."
  default = {
    "TerminationDate" = "Permanent",
    "Environment"     = "Development",
    "Team"            = "DevOps",
    "DeployedBy"      = "Terraform",
    "OwnerEmail"      = "devops@example.com"
  }
}

variable "vpc_name" {
  default     = "cloud_vpc"
  description = "Name for VPC"
  type        = string
}
variable "vpc_cidr" {
  default     = "10.20.0.0/16"
  description = "Cidr for VPC"
  type        = string
}
