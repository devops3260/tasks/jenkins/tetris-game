provider "aws" {
  profile                  = "playground"
  shared_credentials_files = ["~/.aws/credentials"]
  region                   = var.aws_region

  default_tags {
    tags = var.default_tags
  }
}

data "aws_availability_zones" "available" {}
