######################################
# Data search ami ubuntu linux
#####################################

data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = ["099720109477"]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

}

locals {
  user_data = file("${path.module}/jenkins.sh")
}

module "jenkins-vm" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "5.5.0"

  name                        = "Jenkins"
  instance_type               = "t3.medium"
  ami                         = data.aws_ami.ubuntu.id
  key_name                    = aws_key_pair.key.key_name
  monitoring                  = false
  vpc_security_group_ids      = [aws_security_group.jenkins.id]
  subnet_id                   = module.vpc.public_subnets[0]
  associate_public_ip_address = true
  user_data_base64            = base64encode(local.user_data)
  depends_on                  = [module.vpc]
  tags = {
    "Name" = "Jenkins"
  }
}

resource "aws_security_group" "jenkins" {
  name        = "Jenkins-sg"
  description = "Allow trafic for Jenkis EC2"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "SSH"
    from_port   = "22"
    to_port     = "22"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "Jenkins port 8080"
    from_port   = "8080"
    to_port     = "8080"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "HTTP"
    from_port   = "80"
    to_port     = "80"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "SonarQube port 9000"
    from_port   = "9000"
    to_port     = "9000"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}
# Creation SSH-key
resource "aws_key_pair" "key" {
  key_name   = "deployer-key"
  public_key = file("~/.ssh/id_rsa.pub")
}
