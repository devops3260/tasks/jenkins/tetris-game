# Jenkins Project
![Project Diagram](./jenkins_project_diagrams.png)
### Project Structure
In this project we will using Jenkins as a CICD tool and deploying our application on a Docker container.


#### Steps
1. With help terraform we deploy our infrastruction. Also using user data terraform installing Jenkins, Docker and Trivy. Created a Sonarqube Container using Docker.
2. Install plugins Sonarqube Scanner, NodeJS, OWASP Dependency-Check, Docker
3. Create a Pipeline Project in Jenkins using a Declarative Pipeline
4. Install OWASP Dependency Check Plugins
5. Docker Image Build and Push
6. Deploy the image using Docker
